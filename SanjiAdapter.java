public class SanjiAdapter implements AnimeHero {
    private Sanji sanji;
    public SanjiAdapter(Sanji sanji){
        this.sanji = sanji;
    }
    @Override
    public String attack(){
        return this.sanji.kick();
    }
}
